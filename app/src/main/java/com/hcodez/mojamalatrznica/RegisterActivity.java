package com.hcodez.mojamalatrznica;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hcodez.mojamalatrznica.User.User;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {
    private Button btn_register;
    private EditText etxt_email;
    private EditText etxt_password;
    private CheckBox chk_publisher;

    private DatabaseReference mDatabase;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        InitializeUI();
    }

    private void InitializeUI() {
        btn_register = findViewById(R.id.btn_register);
        etxt_email = findViewById(R.id.etxt_email);
        etxt_password = findViewById(R.id.etxt_password);
        chk_publisher = findViewById(R.id.chk_publisher);

        auth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance("https://mylittlemarketplace-b3d6e-default-rtdb.europe-west1.firebasedatabase.app/").getReference();
    }

    public void RegisterClicked(View view) {
        String email = etxt_email.getText().toString();
        String password = etxt_password.getText().toString();

        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Sva polja moraju biti popunjena!", Toast.LENGTH_SHORT).show();
        } else if (password.length() < 6) {
            Toast.makeText(this, "Lozinka mora imati 6 ili vise znakova!", Toast.LENGTH_SHORT).show();
        } else {
            String isPublisher = "false";
            if (chk_publisher.isChecked()) {
                isPublisher = "true";
            }
            RegisterNewUser(email, password, isPublisher);
        }
    }

    private void RegisterNewUser(final String email, final String password, final String isPublisher) {
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            assert firebaseUser != null;
                            String userid = firebaseUser.getUid();

                            Map<String, User> newUser = new HashMap<>();
                            newUser.put(userid, new User(userid, email, password, isPublisher));

                            mDatabase.child("users").setValue(newUser)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(RegisterActivity.this, "Nismo vas mogli registrirati s ovim emailom ili lozinkom!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}