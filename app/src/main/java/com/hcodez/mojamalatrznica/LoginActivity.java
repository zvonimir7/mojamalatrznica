package com.hcodez.mojamalatrznica;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LoginActivity extends AppCompatActivity {
    private EditText etxt_email, etxt_password;
    private Button btn_login;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        InitializeUI();
    }

    private void InitializeUI() {
        etxt_email = findViewById(R.id.etxt_email);
        etxt_password = findViewById(R.id.etxt_password);
        btn_login = findViewById(R.id.btn_login);

        auth = FirebaseAuth.getInstance();
    }

    public void LoginClicked(View view) {
        String email = etxt_email.getText().toString();
        String password = etxt_password.getText().toString();

        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, "All fields must be filled!", Toast.LENGTH_SHORT).show();
        } else {
            SignInWithEmailAndPassword(email, password);
        }
    }

    private void SignInWithEmailAndPassword(String email, String password) {
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            UpdateDataBaseLogInTime(auth.getCurrentUser().getUid());
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(LoginActivity.this, "Authentication failed!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void UpdateDataBaseLogInTime (String currentUserID) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance("https://mylittlemarketplace-b3d6e-default-rtdb.europe-west1.firebasedatabase.app/").getReference();
        String timeStamp;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            timeStamp = dtf.format(now);
        } else {
            timeStamp = "android.os.Build.VERSION lower then API21";
        }

        mDatabase.child("logs").child(currentUserID + "/" + "last time signed in").setValue(timeStamp);
    }
}