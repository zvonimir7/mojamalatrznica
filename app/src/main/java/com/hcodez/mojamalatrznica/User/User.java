package com.hcodez.mojamalatrznica.User;

public class User {
    private String Id;
    private String Email;
    private String Password;
    private String IsPublisher;

    public User () { }

    public User (String id, String email, String password, String isPublisher) {
        this.Id = id;
        this.Email = email;
        this.Password = password;
        this.IsPublisher = isPublisher;
    }

    public String getIsPublisher() {
        return IsPublisher;
    }

    public String getId() {
        return Id;
    }

    public String getEmail() {
        return Email;
    }

    public String getPassword() {
        return Password;
    }
}